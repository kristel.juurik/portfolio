import { Component, OnInit } from '@angular/core';
import { Certificates } from '../certificates';

@Component({
  selector: 'app-certificates',
  templateUrl: './certificates.component.html',
  styleUrls: ['./certificates.component.css']
})
export class CertificatesComponent implements OnInit {
  
  certificatesClicked = false;

  certificates: Certificates[] = [

    { CertificateName: 'Resonsive Web Design', Organization: 'freeCodeCamp', Date: 'Jan 2021', Credential: 'assets/responsivewebdesigncertificate.jpg'},
    { CertificateName: 'Learn Java Course', Organization: 'Codecademy', Date: 'Dec 2020', Credential: 'assets/codecademyjavacertificate.jpg'},
    { CertificateName: 'Software Tester', Organization: 'Software Development Academy', Date: 'Nov 2020', Credential: 'assets/testercertificate.jpg'}
    // { CertificateName: , Organization: , Date: , Credential: }
  ]

  constructor() { }

  ngOnInit(): void {
  }

  handleClick() {
    this.certificatesClicked = !this.certificatesClicked;
  }

}
